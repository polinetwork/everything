# Every Study Course

`Italian Version`

Ciao, questa è la repository dedicata ai corsi di studio gestiti dal PoliNetwork.
Ci troverete materiale riguardante i vari corsi e potrete caricarlo anche voi attraverso il bot telegram @PoliMaterial_bot

- Informatica https://gitlab.com/polinetwork/Info
 
- Aerospaziale https://gitlab.com/polinetwork/AES

- Elettronica https://gitlab.com/polinetwork/elettronica
 
- Materiali e Nanotecnologie https://gitlab.com/polinetwork/MatNano

- Mobility Eng https://gitlab.com/polinetwork/MobilityMD

`English Version`

Hello everyone this is the repository dedicated to PoliNetwork's courses.
You will be able to find and upload materials for your courses (Notes, Exercises, ...).
To upload files use the Telegram Bot @PoliMaterial_bot

- Computer Eng https://gitlab.com/polinetwork/Info
 
- Aerospace https://gitlab.com/polinetwork/AES

- Electronics https://gitlab.com/polinetwork/elettronica
 
- Materials Eng and Nanotechnology https://gitlab.com/polinetwork/MatNano

- Mobility Eng https://gitlab.com/polinetwork/MobilityMD


`DISCLAIMER`
**You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.**

Polinetwork takes no responsibility for and we do not expressly or implicitly endorse, support, or guarantee the completeness, truthfulness, accuracy, or reliability of any of the content on this repository or any of the ones associated to it.
